package com.jankariportal;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.meg7.widget.CircleImageView;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class VisitLogAdapter extends RecyclerView.Adapter<VisitLogAdapter.MyViewHolder> {

    private List<VisitLog> logList = new ArrayList<>();
    Context context;
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_visit,parent,false);
        context = parent.getContext();
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        VisitLog visitLog = logList.get(position);
        holder.cName.setText(visitLog.getClientName());
        holder.inTime.setText(visitLog.getTimeIn());
        holder.outTime.setText(visitLog.getTimeOut());
        Picasso.get().load(logList.get(position).getInImage()).into(holder.inImage);
        Picasso.get().load(logList.get(position).getOutImage()).into(holder.outImage);
//        Picasso.with(context).load(logList.get(position).getInImage()).into(holder.inImage);
        /*try {
            URL url = new URL(visitLog.getInImage());
            holder.inImage.setImageBitmap(BitmapFactory.decodeStream(url.openConnection().getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            URL url = new URL(visitLog.getOutImage());
            holder.outImage.setImageBitmap(BitmapFactory.decodeStream(url.openConnection().getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public int getItemCount() {
        return logList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView cName ,inTime,outTime;
        public CircleImageView inImage,outImage;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            cName = itemView.findViewById(R.id.client_name);
            inTime = itemView.findViewById(R.id.time_in);
            outTime = itemView.findViewById(R.id.time_out);
            inImage = itemView.findViewById(R.id.in_image);
            outImage = itemView.findViewById(R.id.out_image);
        }
    }

    public VisitLogAdapter(List<VisitLog> logs){
        this.logList = logs;
    }


}
