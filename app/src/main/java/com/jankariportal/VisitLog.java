package com.jankariportal;

import android.os.StrictMode;

public class VisitLog {

    private String clientName,timeIn,timeOut,inImage,outImage;
//    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

    public VisitLog(String clientName, String timeIn, String timeOut, String inImage, String outImage) {
        this.clientName = clientName;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
        this.inImage = inImage;
        this.outImage = outImage;
//        StrictMode.setThreadPolicy(policy);
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(String timeIn) {
        this.timeIn = timeIn;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }

    public String getInImage() {
        return inImage;
    }

    public void setInImage(String inImage) {
        this.inImage = inImage;
    }

    public String getOutImage() {
        return outImage;
    }

    public void setOutImage(String outImage) {
        this.outImage = outImage;
    }
}
