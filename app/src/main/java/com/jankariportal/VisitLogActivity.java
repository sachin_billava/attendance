package com.jankariportal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toolbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class VisitLogActivity extends AppCompatActivity {
    private EditText dateSelector;
    private RecyclerView recyclerView;
    private Calendar calendar;
    List<VisitLog> visitLogList = new ArrayList<>();
    View progress;
    VisitLogAdapter adapter = new VisitLogAdapter(visitLogList);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit_log);
        setTitle("Visit Log");
        final Context context = getApplicationContext();
        dateSelector = findViewById(R.id.dateSelector);
        calendar = Calendar.getInstance();
        progress = findViewById(R.id.progress_layout);
        recyclerView = findViewById(R.id.recycler_view);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR,year);
                calendar.set(Calendar.MONTH,month);
                calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                updateEditTextDate(dateSelector);
            }

        };
        dateSelector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(VisitLogActivity.this,R.style.DatePickerDialogTheme,dateSetListener,calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        String date = new SimpleDateFormat("EEE, d-MMM-yyyy", Locale.getDefault()).format(new Date());
        dateSelector.setText("Date : "+date);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void updateEditTextDate(EditText editText) {
        String format = "dd/MM/yyyy";
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        String date = simpleDateFormat.format(calendar.getTime());
        editText.setText("Date : "+date);
        visitLogList.clear();
        showLogs(date);
        progress.setVisibility(View.VISIBLE);
    }

    private void showLogs(String date) {

        recyclerView.setVisibility(View.VISIBLE);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getApplicationContext(),DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.divider));
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(adapter);

        new GetValuesFromJSON(date).execute();
    }


    private class GetValuesFromJSON extends AsyncTask<Void,Void,Void>{

        public GetValuesFromJSON(String date) {
            this.date = date;
        }
        String date;
        @Override
        protected Void doInBackground(Void... voids) {

            JSONHandler jsonHandler = new JSONHandler();
            String jsonStr = jsonHandler.makeJSONCall("https://my-json-server.typicode.com/ashu0211/demoJSON/visitlogs");
            if (jsonStr != null){
                try {
                    JSONArray jsonArray = new JSONArray(jsonStr);
                    for (int i = 0; i<jsonArray.length();i++){
                        JSONObject jsonObject= jsonArray.getJSONObject(i);
                        if (date.equals(jsonObject.getString("date"))){
                            VisitLog visitLog = new VisitLog(jsonObject.getString("person"),jsonObject.getString("visitin"),jsonObject.getString("visitout"),"https://picsum.photos/50","https://picsum.photos/50");
                            visitLogList.add(visitLog);
                        }else{
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

                    adapter.notifyDataSetChanged();
                    progress.setVisibility(View.GONE);

        }
    }
}
