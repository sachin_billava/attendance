package com.jankariportal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    Button loginBtn;
    private static final int LOCATION_CODE = 100;
    private static final int CAMERA_CODE = 101;
    private static final int INTERNET_CODE = 102;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginBtn = findViewById(R.id.loginBtn);
        requestAllPermissions();
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
    private void requestAllPermissions() {
        checkPermission(Manifest.permission.ACCESS_FINE_LOCATION,LOCATION_CODE);
        checkPermission(Manifest.permission.INTERNET,INTERNET_CODE);
        checkPermission(Manifest.permission.CAMERA,CAMERA_CODE);
    }

    private void checkPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(this,permission) == PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(this,new String[]{permission},requestCode);
        }else {
            Toast.makeText(getApplicationContext(), "Permission already granted", Toast.LENGTH_SHORT).show();
        }
    }
}
