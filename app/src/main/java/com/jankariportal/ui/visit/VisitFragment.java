package com.jankariportal.ui.visit;


import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jankariportal.AppLocationService;
import com.jankariportal.LocationAddress;
import com.jankariportal.R;
import com.jankariportal.ShowSettingsAlert;
import com.jankariportal.VisitLogActivity;
import com.meg7.widget.CircleImageView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.app.Activity.RESULT_CANCELED;

public class VisitFragment extends Fragment {

    private Spinner spinner;
    private TextView address;

    private CheckBox checkBox;
    private Button visitIn,visitOut;
    private CircleImageView image;
    private int Camera = 2;
    boolean flag;
    Calendar calendar;
    AppLocationService appLocationService;
    View root;
    private FloatingActionButton fab;

    public VisitFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_visit, container, false);
        spinner = root.findViewById(R.id.spinner);
        address = root.findViewById(R.id.address);
        checkBox = root.findViewById(R.id.outCheck);
        checkBox.setVisibility(View.GONE);
        visitIn = root.findViewById(R.id.visitIn);
        fab = root.findViewById(R.id.floatingActionButton);
        appLocationService = new AppLocationService(getActivity());
        image = root.findViewById(R.id.profilePicture);
        visitOut = root.findViewById(R.id.visitOut);
        visitOut.setVisibility(View.GONE);
        calendar = Calendar.getInstance();
        flag = true;
        fetchAddress();
        selectSpinnerData(spinner);
        visitIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                captureImageFromCamera(v);
            }
        });
        visitOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                captureImageFromCamera(v);
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), VisitLogActivity.class);
                startActivity(intent);
            }
        });
        return root;
    }
    private void selectSpinnerData(Spinner spinner) {
        List<String> list = new ArrayList<>();
        list.add("Select Visit Type");
        list.add("School Visit");
        list.add("College Visit");
        list.add("Company Visit");
        ArrayAdapter arrayAdapter = new ArrayAdapter(getContext(),android.R.layout.simple_spinner_dropdown_item,list);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
    }

    private void fetchAddress() {
        Location location = appLocationService.getLocation(LocationManager.NETWORK_PROVIDER);
        if (location != null) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            LocationAddress locationAddress = new LocationAddress();
            locationAddress.getAddressFromLocation(latitude, longitude, getActivity(), new GeocoderHandler());
        } else {
            new ShowSettingsAlert(getContext());
        }
    }

    private void captureImageFromCamera(View v){
        if (v == visitIn){
            flag = true;
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, Camera);
        }
        if (v == visitOut){
            flag = false;
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, Camera);
        }
    }
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("Code", String.valueOf(requestCode));
        if (resultCode == RESULT_CANCELED) {
            fetchAddress();
            return;
        }
        if (requestCode == Camera) {
            fetchAddress();
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            Log.e("thum",thumbnail.toString());
            image.setImageBitmap(thumbnail);
            if (flag){
                visitIn.setVisibility(View.GONE);
                visitOut.setVisibility(View.VISIBLE);
                checkBox.setVisibility(View.VISIBLE);
            }else {
                visitIn.setVisibility(View.VISIBLE);
                visitOut.setVisibility(View.GONE);
                checkBox.setVisibility(View.GONE);

            }
            saveImage(thumbnail);
            Toast.makeText(getContext(), "Image Saved!", Toast.LENGTH_SHORT).show();
        }
    }
    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
            Log.i("add",locationAddress);
            address.setText(locationAddress);
        }
    }
    private String saveImage(Bitmap bitmap) {
        return "";
    }

}
