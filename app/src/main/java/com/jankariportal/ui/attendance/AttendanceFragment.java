package com.jankariportal.ui.attendance;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jankariportal.AppLocationService;
import com.jankariportal.AttendanceLogActivity;
import com.jankariportal.LocationAddress;
import com.jankariportal.R;
import com.jankariportal.ShowSettingsAlert;
import com.meg7.widget.CircleImageView;

import static android.app.Activity.RESULT_CANCELED;

/**
 * A simple {@link Fragment} subclass.
 */
public class AttendanceFragment extends Fragment {

    private int Camera =2;
    CircleImageView takeImage;
    Button markIn,markOut;
    boolean flag;
    View root;
    ImageView refreshIcon;
    TextView addressTextView;
    TextView clickToRefresh;
    AppLocationService appLocationService;
//    ProgressDialog progressBar;
    View progress;

    public AttendanceFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_attendance, container, false);
        takeImage = root.findViewById(R.id.profilePicture);
//        progressBar = new ProgressDialog(getContext());
//        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        clickToRefresh = root.findViewById(R.id.clickToRefresh);
        markIn = root.findViewById(R.id.markin);
        markOut = root.findViewById(R.id.markout);
        markOut.setVisibility(View.GONE);
        progress = root.findViewById(R.id.progress_layout);
        progress.setVisibility(View.VISIBLE);
        refreshIcon = root.findViewById(R.id.refresh);
        addressTextView = root.findViewById(R.id.address);
        appLocationService = new AppLocationService(getActivity());
        flag = true;
        fetchAddress();
        markIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                captureImageFromCamera(v);
            }
        });
        markOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                captureImageFromCamera(v);
            }
        });
        clickToRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress.setVisibility(View.VISIBLE);
                Animation animation = AnimationUtils.loadAnimation(getContext(),R.anim.rotate);
                refreshIcon.startAnimation(animation);
                fetchAddress();
            }
        });
        refreshIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress.setVisibility(View.VISIBLE);
                Animation animation = AnimationUtils.loadAnimation(getContext(),R.anim.rotate);
                refreshIcon.startAnimation(animation);
                fetchAddress();
            }
        });
        FloatingActionButton fab = root.findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                progressBar.setTitle("Loading Logs!!");
//                progressBar.setMessage("Please Wait!");
//                progressBar.show();
                progress.setVisibility(View.VISIBLE);
                Intent intent = new Intent(getActivity(), AttendanceLogActivity.class);
                startActivity(intent);
            }
        });
        return root;
    }

    private void fetchAddress() {
        Location location = appLocationService.getLocation(LocationManager.NETWORK_PROVIDER);
        if (location != null) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            LocationAddress locationAddress = new LocationAddress();
            locationAddress.getAddressFromLocation(latitude, longitude, getActivity(), new GeocoderHandler());
            progress.setVisibility(View.GONE);
        } else {
            new ShowSettingsAlert(getContext());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        progressBar.dismiss();
        progress.setVisibility(View.GONE);
    }

    private void captureImageFromCamera(View v){
        if (v == markIn){
            flag = true;
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, Camera);
        }
        if (v == markOut){
            flag = false;
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, Camera);
        }
    }
    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
            Log.i("add",locationAddress);
            addressTextView.setText(locationAddress);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("Code", String.valueOf(requestCode));
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (requestCode == Camera) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            Log.e("thum",thumbnail.toString());
            takeImage.setImageBitmap(thumbnail);
            if (flag){
                markIn.setVisibility(View.GONE);
                markOut.setVisibility(View.VISIBLE);
            }else {
                markIn.setVisibility(View.VISIBLE);
                markOut.setVisibility(View.GONE);

            }
            saveImage(thumbnail);
            Toast.makeText(getContext(), "Image Saved!", Toast.LENGTH_SHORT).show();
        }
    }

    private String saveImage(Bitmap bitmap) {
        return "";
    }


}
