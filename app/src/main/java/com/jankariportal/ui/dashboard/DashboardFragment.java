package com.jankariportal.ui.dashboard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.jankariportal.MainActivity;
import com.jankariportal.R;
import com.jankariportal.ui.attendance.AttendanceFragment;
import com.jankariportal.ui.profile.ProfileFragment;
import com.jankariportal.ui.visit.VisitFragment;

import java.util.ArrayList;

public class DashboardFragment extends Fragment {

    ActionBar actionBar;
    private View root;
    private BottomNavigationView bottomNavigationView;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_dashboard, container, false);

        bottomNavigationView = root.findViewById(R.id.bottom_nav);
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        openFragment(new ProfileFragment());
        actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        return root;
    }

    private void openFragment(Fragment fragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.navigation_profile:
                            openFragment(new ProfileFragment());
                            actionBar.setTitle("Profile");
                            return true;
                        case R.id.navigation_attendance:
                            openFragment(new AttendanceFragment());
                            actionBar.setTitle("Attendance");
                            return true;
                        case R.id.navigation_visit:
                            openFragment(new VisitFragment());
                            actionBar.setTitle("Visits");
                            return true;
                    }
                    return false;
                }
            };

}