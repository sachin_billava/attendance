package com.jankariportal.ui.profile;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.RippleDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jankariportal.JSONHandler;
import com.jankariportal.MainActivity;
import com.jankariportal.R;
import com.meg7.widget.CircleImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_CANCELED;

public class ProfileFragment extends Fragment {

    TextView name,designation,salary,shift_timing,department,mobile,email;
    CircleImageView profile_image;
    Map<String,String> data;
    View root;
    Button update;
    private int Camera =2;
    private int Gallery = 1;
    View dialogView;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    View progress;
    FloatingActionButton changeProfile;
    private static final String IMAGE_DIRECTORY = "/portal/";
    public ProfileFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_profile, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        builder = new AlertDialog.Builder(getContext());
        dialogView = inflater.inflate(R.layout.update_mobile_alert,null);
        builder.setCancelable(true);
        builder.setView(dialogView);
        progress = root.findViewById(R.id.progress_layout);
        progress.setVisibility(View.VISIBLE);
        name = root.findViewById(R.id.name);
        designation = root.findViewById(R.id.designation);
        salary = root.findViewById(R.id.salary);
        shift_timing = root.findViewById(R.id.shift);
        department = root.findViewById(R.id.department);
        mobile = root.findViewById(R.id.mobile_no);
        email = root.findViewById(R.id.email);
        changeProfile = root.findViewById(R.id.changeProIcon);

        profile_image = root.findViewById(R.id.profilePicture);
        changeProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                captureImageFromCamera();
                showPictureDialog();
            }
        });
        data = new HashMap<>();
        update = root.findViewById(R.id.updateMobile);
        dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openMobileDialog(mobile.getText().toString().trim());
            }
        });
        new GetValuesFromJSON().execute();

        return root;
    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getContext());
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera" };
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                captureImageFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }
    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, Gallery);
    }
    private void openMobileDialog(String number) {
        dialog.show();
        final EditText numberET = dialogView.findViewById(R.id.d_mobile);
        numberET.setText(number);
        Button cancel,update;
        cancel = dialogView.findViewById(R.id.d_cancel);
        update = dialogView.findViewById(R.id.d_update);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                mobile.setText(numberET.getText().toString().trim());
            }
        });
    }


    private void setData() {
        name.setText(data.get("name"));
        designation.setText(data.get("designation"));
        salary.setText(data.get("salary"));
        shift_timing.setText(data.get("shift_and_timing"));
        department.setText(data.get("department"));
        mobile.setText(data.get("mobile"));
        email.setText(data.get("email"));

    progress.setVisibility(View.GONE);
    }

    private void captureImageFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, Camera);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("Code", String.valueOf(requestCode));
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (requestCode == Camera) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            Log.e("thum",thumbnail.toString());
            profile_image.setImageBitmap(thumbnail);
            saveImage(thumbnail);
            Toast.makeText(getContext(), "Image Saved!", Toast.LENGTH_SHORT).show();

        }else if (requestCode == Gallery) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentURI);
                    String path = saveImage(bitmap);
                    Toast.makeText(getContext(), "Image Saved!", Toast.LENGTH_SHORT).show();
                    profile_image.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        }



    }

    private String saveImage(Bitmap bitmap) {
        return "";
    }
    private class GetValuesFromJSON extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            JSONHandler jsonHandler = new JSONHandler();
            String jsonStr = jsonHandler.makeJSONCall("https://my-json-server.typicode.com/ashu0211/demoJSON/user");
            if (jsonStr != null){
                try {
                    JSONObject jsonObject = new JSONObject(jsonStr);
                    data.put("name",jsonObject.getString("name"));
                    data.put("designation",jsonObject.getString("designation"));
                    data.put("salary",jsonObject.getString("salary"));
                    data.put("shift_and_timing",jsonObject.getString("shift_and_timing"));
                    data.put("department",jsonObject.getString("department"));
                    data.put("email",jsonObject.getString("email"));
                    data.put("mobile",jsonObject.getString("mobile"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                Toast.makeText(getContext(),"PROBLEM!!!",Toast.LENGTH_SHORT).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            setData();
        }
    }
}
