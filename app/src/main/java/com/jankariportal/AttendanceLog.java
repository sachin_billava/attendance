package com.jankariportal;

import android.os.StrictMode;
import android.util.Log;

import java.net.URL;

public class AttendanceLog {
//    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

    private String date;
    private String address;
    private String imgInUrl,imgOutUrl;
    private String timeIn;
    private String timeOut;

    public AttendanceLog(String date, String address, String imgInUrl, String imgOutUrl, String timeIn, String timeOut) {
//        StrictMode.setThreadPolicy(policy);
        this.date = date;
        this.address = address;
        this.imgInUrl = imgInUrl;
        this.imgOutUrl = imgOutUrl;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImgInUrl() {
        return imgInUrl;
    }

    public void setImgInUrl(String imgInUrl) {
        this.imgInUrl = imgInUrl;
    }

    public String getImgOutUrl() {
        return imgOutUrl;
    }

    public void setImgOutUrl(String imgOutUrl) {
        this.imgOutUrl = imgOutUrl;
    }

    public String getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(String timeIn) {
        this.timeIn = timeIn;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }
}
