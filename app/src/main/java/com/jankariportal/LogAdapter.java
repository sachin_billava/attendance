package com.jankariportal;

import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.meg7.widget.CircleImageView;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.URL;
import java.util.List;

public class LogAdapter  extends RecyclerView.Adapter<LogAdapter.LogHolder> {
    private List<AttendanceLog> logs;
    private URL url = null;
    public class LogHolder extends RecyclerView.ViewHolder{
        public TextView date,timeIn,timeOut,address;
        public CircleImageView imageIn,imageOut;
        public LogHolder(@NonNull View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.dateLog);
            timeIn = itemView.findViewById(R.id.timeInLog);
            timeOut = itemView.findViewById(R.id.timeOutLog);
            address = itemView.findViewById(R.id.addressLog);
            imageIn = itemView.findViewById(R.id.inImage);
            imageOut = itemView.findViewById(R.id.outImage);
        }
    }

    public LogAdapter(List<AttendanceLog> logs) {
        this.logs = logs;
    }
    @NonNull
    @Override
    public LogHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_logs,parent,false);
        return new LogHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LogHolder holder, int position) {
        AttendanceLog attendanceLog = logs.get(position);
        holder.date.setText(attendanceLog.getDate());
        holder.timeIn.setText(attendanceLog.getTimeIn());
        holder.timeOut.setText(attendanceLog.getTimeOut());
        holder.address.setText(attendanceLog.getAddress());
        Picasso.get().load(logs.get(position).getImgInUrl()).into(holder.imageIn);
        Picasso.get().load(logs.get(position).getImgOutUrl()).into(holder.imageOut);
       /* try {
            url= new URL(attendanceLog.getImgInUrl());
            holder.imageIn.setImageBitmap(BitmapFactory.decodeStream(url.openConnection().getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            url= new URL(attendanceLog.getImgOutUrl());
            holder.imageOut.setImageBitmap(BitmapFactory.decodeStream(url.openConnection().getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public int getItemCount() {
        return logs.size();
    }

}
