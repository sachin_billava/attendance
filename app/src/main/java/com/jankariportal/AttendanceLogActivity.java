package com.jankariportal;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AttendanceLogActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<AttendanceLog> logList = new ArrayList<>();
    private LogAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_log);
        setTitle("Attendance Log");
        recyclerView =findViewById(R.id.recycler_view);
        adapter = new LogAdapter(logList);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getApplicationContext(),DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.divider));
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(adapter);

        new GetValuesFromJSON().execute();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
    private class GetValuesFromJSON extends AsyncTask<Void,Void,Void> {
        JSONObject jsonObject;
        @Override
        protected Void doInBackground(Void... voids) {
            JSONHandler jsonHandler = new JSONHandler();

            String jsonStr = jsonHandler.makeJSONCall("https://my-json-server.typicode.com/ashu0211/demoJSON/logs");
            if (jsonStr != null){
                try {
                    JSONArray jsonArray = new JSONArray(jsonStr);
                    for (int i = 0; i<jsonArray.length();i++){
                        jsonObject = jsonArray.getJSONObject(i);
                        AttendanceLog attendanceLog = new AttendanceLog(jsonObject.getString("date"),jsonObject.getString("address"),"https://picsum.photos/50","https://picsum.photos/50",jsonObject.getString("timein"),jsonObject.getString("timeout"));
                        logList.add(attendanceLog);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                Toast.makeText(getApplicationContext(),"PROBLEM!!!",Toast.LENGTH_SHORT).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            adapter.notifyDataSetChanged();
        }
    }

}
